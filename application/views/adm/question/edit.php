<div class="container">
    <h1><?php echo (isset($data->id) ? 'Editar' : 'Novo') . ' ' . 'post' ?> </h1>
    <form role="form" method="post" enctype="multipart/form-data" action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/salvar/<?php echo isset($data->id) ? $data->id : NULL ?>">
        <div class="form-group">
            <label for="category_id">Categoria: </label>
            <select name="category_id" id="category_id" class="form-control">
                <?php foreach($category as $row):?>
                <option value="<?php echo $row->id; ?>" <?php echo isset($data->category_id) ? 'selected' : NULL ?>><?php echo $row->name; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="name">Pergunta: </label>
            <input class="form-control" type="text" name="name" id="name" value="<?php echo isset($data->name) ? $data->name : NULL ?>" required="" />
        </div>
        <div class="row">
            <div class="col-md-10 col-xs-7">
                <div class="form-group">
                    <label for="alternative1">Alternativa 1: </label>
                    <input class="form-control" type="text" name="alternative[]" id="alternative1" value="<?php echo isset($data->alternative1) ? $data->alternative1 : NULL ?>" required="" />
                </div>
            </div>
            <div class="col-md-2 col-xs-5">
                <div class="form-group">
                    <label for="status1">Status: </label>
                    <select name="status[]" id="status1" class="form-control">
                        <option value="1">Correta</option>
                        <option value="0">Errada</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-xs-7">
                <div class="form-group">
                    <label for="alternative1">Alternativa 2: </label>
                    <input class="form-control" type="text" name="alternative[]" id="alternative2" value="<?php echo isset($data->alternative2) ? $data->alternative2 : NULL ?>" required="" />
                </div>
            </div>
            <div class="col-md-2 col-xs-5">
                <div class="form-group">
                    <label for="status2">Status: </label>
                    <select name="status[]" id="status2" class="form-control">
                        <option value="1">Correta</option>
                        <option value="0">Errada</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-xs-7">
                <div class="form-group">
                    <label for="alternative3">Alternativa 3: </label>
                    <input class="form-control" type="text" name="alternative[]" id="alternative3" value="<?php echo isset($data->alternative3) ? $data->alternative3 : NULL ?>" required="" />
                </div>
            </div>
            <div class="col-md-2 col-xs-5">
                <div class="form-group">
                    <label for="status3">Status: </label>
                    <select name="status[]" id="status3" class="form-control">
                        <option value="1">Correta</option>
                        <option value="0">Errada</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="Salvar">
        </div>
    </form>
</div>
