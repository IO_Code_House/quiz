<div class="container">
    <table class="dataTable">
        <thead>
        <tr>
            <th>Categoria</th>
            <th>Pergunta</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($list as $row) :?>
            <tr>
                <td><?php echo $row->category_name; ?></td>
                <td><?php echo $row->name; ?></td>
                <td class="action">
                    <a href="./adm/<?php echo $this->uri->segment(2); ?>/editar/<?php echo $row->id; ?>">
                        <img width="29" height="29" src= "./assets/adm/img/edit.png" title="Editar" />
                    </a>
                    <a  href="./adm/<?php echo $this->uri->segment(2); ?>/excluir/<?php echo $row->id; ?>">
                        <img width="29" height="29" src= "./assets/adm/img/delete.png" title="Excluir" />
                    </a>
                </td>
            </tr>
        <?php endForeach;?>
        </tbody>
    </table>
</div>