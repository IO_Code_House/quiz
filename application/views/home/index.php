<html>
<head>
    <title>quiz_claro_onepage</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- Save for Web Slices (quiz_claro_onepage.psd) -->
<form method="POST">
    <table id="Tabela_01" width="1000" height="6501" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td >
                <img src="assets/images/quiz_claro_onepage_01.png" width="1000" height="131" alt="" style="display: block; border: 0;"></td>
        </tr>
        <tr>
            <td >

                <img src="assets/images/quiz_claro_onepage_02.png" width="1000" height="341" alt="" style="display: block; border: 0;"></td>
        </tr>
        <tr>
            <td >
                <a href="#comecar">
                    <img src="assets/images/quiz_claro_one_02.png" width="1000" height="354" alt="" style="display: block; border: 0;">
                </a>
            </td>
        </tr>
        <tr>
            <td >
                <a name="comecar"></a>
                <img src="assets/images/quiz_claro_onepage_04.png" width="1000" height="336" alt="" style="display: block; border: 0;">
            </td>

        </tr>
        <tr>
            <td valign="top" style="padding-left: 2px">
                <table bgcolor="#0a6083" width="920" align="center" height="368">
                    <tr>
                        <td valign="top">
                            <table width="800" align="center" height="368">
                                <tr>
                                    <td>
                                        <h3 style="font-size: 26px; margin: 0; "><font face="Arial" color="white"><?php echo $questions[0]->question; ?></font> </h3>
                                        <br>
                                        <?php foreach ($questions[0]->alternatives  as $key => $row): ?>
                                            <input type="radio" name="tesp" value="<?php echo $row->status ?>" id="<?php echo $row->id ?>" required>
                                            <font face="Arial" size="5" color="white"><i><label for="<?php echo $row->id ?>"><?php echo $row->text ?></label></i></font></br></br>
                                        <?php endforeach; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>

            <td >
                <img src="assets/images/quiz_claro_onepage_06.png" width="1000" height="335" alt="" style="display: block; border: 0;">
            </td>
        </tr>
        <tr>
            <td height="364" valign="top" style="padding-left: 2px">
                <table bgcolor="#bb890d" width="920" align="center" height="364">
                    <tr>
                        <td valign="top">
                            <table width="800" align="center" height="368">
                                <tr>
                                    <td>
                                        <h3 style="font-size: 26px; margin: 0; "><font face="Arial" color="white"><?php echo $questions[1]->question; ?></font> </h3>
                                        <br>
                                        <?php foreach ($questions[1]->alternatives  as $key => $row): ?>
                                            <input type="radio" name="rai" value="<?php echo $row->status ?>" id="<?php echo $row->id ?>" required>
                                            <font face="Arial" size="5" color="white"><i><label for="<?php echo $row->id ?>"><?php echo $row->text ?></label></i></font></br></br>
                                        <?php endforeach; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>

        </tr>
        <tr>
            <td >
                <img src="assets/images/quiz_claro_onepage_08.png" width="1000" height="334" alt="" style="display: block; border: 0;"></td>
        </tr>
        <tr>
            <td valign="top" style="padding-left: 2px">
                <table bgcolor="#0cc" width="920" align="center" height="363">
                    <tr>
                        <td valign="top">
                            <table width="800" align="center" height="368">
                                <tr>
                                    <td>
                                        <h3 style="font-size: 26px; margin: 0; "><font face="Arial" color="white"><?php echo $questions[2]->question; ?></font> </h3>
                                        <br>
                                        <?php foreach ($questions[2]->alternatives  as $key => $row): ?>
                                            <input type="radio" name="paq" value="<?php echo $row->status ?>" id="<?php echo $row->id ?>" required>
                                            <font face="Arial" size="5" color="white"><i><label for="<?php echo $row->id ?>"><?php echo $row->text ?></label></i></font></br></br>
                                        <?php endforeach; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td >
                <img src="assets/images/quiz_claro_onepage_10.png" width="1000" height="329" alt="" style="display: block; border: 0;"></td>
        </tr>
        <tr>
            <td valign="top" style="padding-left: 2px">
                <table bgcolor="#936" width="920" align="center" height="380">
                    <tr>
                        <td valign="top">
                            <table width="800" align="center" height="368">
                                <tr>
                                    <td>
                                        <h3 style="font-size: 26px; margin: 0; "><font face="Arial" color="white"><?php echo $questions[3]->question; ?></font> </h3>
                                        <br>
                                        <?php foreach ($questions[3]->alternatives  as $key => $row): ?>
                                            <input type="radio" name="sdq" value="<?php echo $row->status ?>" id="<?php echo $row->id ?>" required>
                                            <font face="Arial" size="5" color="white"><i><label for="<?php echo $row->id ?>"><?php echo $row->text ?></label></i></font></br></br>
                                        <?php endforeach; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td >
                <img src="assets/images/quiz_claro_onepage_12.png" width="1000" height="301" alt="" style="display: block; border: 0;"></td>
        </tr>
        <tr>
            <td valign="top" style="padding-left: 2px">
                <table bgcolor="#063" width="920" align="center" height="398">
                    <tr>
                        <td valign="top">
                            <table width="800" align="center" height="368">
                                <tr>
                                    <td>
                                        <h3 style="font-size: 26px; margin: 0; "><font face="Arial" color="white"><?php echo $questions[4]->question; ?></font> </h3>
                                        <br>
                                        <?php foreach ($questions[4]->alternatives  as $key => $row): ?>
                                            <input type="radio" name="mon" value="<?php echo $row->status ?>" id="<?php echo $row->id ?>" required>
                                            <font face="Arial" size="5" color="white"><i><label for="<?php echo $row->id ?>"><?php echo $row->text ?></label></i></font></br></br>
                                        <?php endforeach; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td >
                <img src="assets/images/quiz_claro_onepage_14.png" width="1000" height="315" alt="" style="display: block; border: 0;"></td>
        </tr>
        <tr>
            <td  valign="top" style="padding-left: 2px">
                <table bgcolor="#c00" width="920" align="center" height="386">
                    <tr>
                        <td valign="top">
                            <table width="800" align="center" height="368">
                                <tr>
                                    <td>
                                        <h3 style="font-size: 26px; margin: 0; "><font face="Arial" color="white"><?php echo $questions[5]->question; ?></font> </h3>
                                        <br>
                                        <?php foreach ($questions[5]->alternatives  as $key => $row): ?>
                                            <input type="radio" name="gen" value="<?php echo $row->status ?>" id="<?php echo $row->id ?>" required>
                                            <font face="Arial" size="5" color="white"><i><label for="<?php echo $row->id ?>"><?php echo $row->text ?></label></i></font></br></br>
                                        <?php endforeach; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td >
                <img src="assets/images/quiz_claro_onepage_16.png" width="1000" height="363" alt="" style="display: block; border: 0;"></td>
        </tr>
        <tr>
            <td  valign="top" style="padding-left: 4px">
                <table bgcolor="#930" width="920" align="center" height="341">
                    <tr>
                        <td valign="top">
                            <table width="800" align="center" height="368">
                                <tr>
                                    <td>
                                        <h3 style="font-size: 26px; margin: 0; "><font face="Arial" color="white"><?php echo $questions[6]->question; ?></font> </h3>
                                        <br>
                                        <?php foreach ($questions[6]->alternatives  as $key => $row): ?>
                                            <input type="radio" name="lean" value="<?php echo $row->status ?>" id="<?php echo $row->id ?>" required>
                                            <font face="Arial" size="5" color="white"><i><label for="<?php echo $row->id ?>"><?php echo $row->text ?></label></i></font></br></br>
                                        <?php endforeach; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td >
                <img src="assets/images/quiz_claro_onepage_18.png" width="1000" height="313" alt="" style="display: block; border: 0;"></td>
        </tr>
        <tr>
            <td  valign="top" style="padding-left: 2px">
                <table bgcolor="#066" width="920" align="center" height="406">
                    <tr>
                        <td valign="top">
                            <table width="800" align="center" height="368">
                                <tr>
                                    <td>
                                        <h3 style="font-size: 26px; margin: 0; "><font face="Arial" color="white"><?php echo $questions[7]->question; ?></font> </h3>
                                        <br>
                                        <?php foreach ($questions[7]->alternatives  as $key => $row): ?>
                                            <input type="radio" name="5s" value="<?php echo $row->status ?>" id="<?php echo $row->id ?>" required>
                                            <font face="Arial" size="5" color="white"><i><label for="<?php echo $row->id ?>"><?php echo $row->text ?></label></i></font></br></br>
                                        <?php endforeach; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="">
                                <button type="submit" style="margin-left: 60px; margin-bottom: 50px; background-color: #000; border-color: #000; color: #fff; font-size: 28px; height: 40px; width: 210px;">Enviar</button>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td >
                <img src="assets/images/quiz_claro_onepage_20.png" width="1000" height="43" alt="" style="display: block; border: 0;"></td>
        </tr>
    </table>
</form>
<!-- End Save for Web Slices -->
</body>
</html>