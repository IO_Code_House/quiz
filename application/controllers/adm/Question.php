<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Question extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('question_model');
    }
    private function load_category()
    {
        $this->load->model('category_model');
        $this->data['category'] = $this->category_model->get()->result();
    }
    public function index()
    {
        $this->load_datatables();
        $this->data['list'] = $this->question_model->get_related()->result();

        parent::renderer();
    }
    public function edit($id = NULL){

        if($this->uri->segment(3) == 'editar' && (int) $id === 0) {
            redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/novo');
        }elseif($id > 0){
            $data = $this->question_model->get(array('id' => $id))->result();
            if(count($data) > 0){
                $data = current($data);
                $this->data['data'] = $data;
            }
        }
        $this->load_category();

        parent::renderer();
    }
    private function load_alternative($question_id)
    {
        $this->load->model('alternative_model');

        $alternative = $this->alternative_model->get(array(
            'question_id' => $question_id,
        ))->result();


        $this->data['alternative'] = array();

        foreach ($alternative as $row) {
            //$this->data['data']->alternative
            $this->data['alternative'][$row->id] = $row;
        }

    }
    public function record($id = NULL)
    {
        $id = (int)$id;
        if($this->input->post()){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('category_id', 'Categoria', 'trim|required');
            $this->form_validation->set_rules('name', 'Pergunta', 'trim|required');

            if($this->form_validation->run() === FALSE){
                $this->setError(validation_errors());
                if($id === 0){
                    $redirect = '/nova';
                }else{
                    $redirect = '/editar/' . $id;
                }
                redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) .  $redirect);
            } else {
                $question = array(
                    'category_id' => $this->input->post('category_id'),
                    'name' => $this->input->post('name'),
                );

                if($id === 0){
                    $id = $this->question_model->insert($question, true);
                }else{
                    $this->question_model->update(array('id' => $id), $question);
                }
                if($id === 0) {
                    $this->setError('Tenho todas informações, mas não consegui gravar. Preciso analisar meus logs');
                }else{
                    $this->load->model('alternative_model');
                    $this->alternative_model->delete(array(
                        'question_id' => $id,
                    ));
                    $alternative = $this->input->post('alternative');
                    $status = $this->input->post('status');


                    foreach ($alternative as $key => $row) {
                        $this->alternative_model->insert(array(
                            'text' => $row,
                            'status' => $status[$key],
                            'question_id' => $id,
                        ));
                    }
                    $this->setMsg('Guardei essas informações, quando precisar é só pedir');
                }
            }
        }else{
            $this->setError('Ocorreu um erro ao processar o formulario, tente novamente mais tarde');
        }
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
    public function delete($id)
    {
        $tempVar = $this->question_model->get(array('id' => $id))->result();
        $tempVar = current($tempVar);
        @unlink($tempVar->img);

        $this->question_model->delete(array('id' => $id));
        $this->setMsg('Joguei essas informações fora, não venha me perguntar sobre elas no futuro...');
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
}