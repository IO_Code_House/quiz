<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ouro extends MY_Controller
{

	public function index()
	{
		$ouro = $this->session->flashdata('gold');

		if($ouro){
			$this->header = null;
			$this->footer = null;
        	$this->renderer();
		}else{
			redirect(base_url(), 'refresh');
		}
		
	}

	public function save()
	{
		if($this->input->post()){
			$this->load->model('gold_model');
			
			$data['name'] 	= $this->input->post('name');
			$data['email'] 	= $this->input->post('email');
			$data['cpf']	= $this->input->post('cpf');
			
			if($this->gold_model->insert($data, true)){
				redirect(base_url(), 'refresh');
			};
		}else{
			redirect(base_url(), 'refresh');
		}
		
	}
}
