<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Question_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_related(array $where = array(), $limit = null)
    {
        $this->db->join('category', $this->getAlias() . '.category_id = category.id');
        $this->db->select('category.name AS category_name');
        return $this->get($where, $limit);
    }

    public function get(array $where = array(), $limit = null)
    {
        $this->db->select($this->getAlias() . '.*');
        return parent::get($where, $limit);
    }
}